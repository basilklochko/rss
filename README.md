# README #

RSS Feed Reader

### Technology Stack ###

* Node Js
* MongoDb
* TypeScript
* Angular JS 4 / Angular CLI
* Google Auth
* Websocket
* Bootstrap

### Deployed To ###

https://rssfeedreader.herokuapp.com/
